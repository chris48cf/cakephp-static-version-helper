<?php
namespace Chris48cf\StaticVersionHelper\View\Helper;

use Cake\Cache\Cache;

/**
 * View helper trait with methods relating to cache-busting
 *
 * This trait overrides standard Html->css() and Html->script()
 * with versions that can (optionally) accept a key 'versioned'
 * to ensure when we change static css + js, browsers
 * download new versions instead of using cached assets
 *
 * This is based on generating a hash value: static_version
 * which is a md5 hash of a directory listing of WWW_ROOT
 * (including last modified timestamps).
 * This means we'll get a new hash value every time something
 * in WWW_ROOT changes. Once generated, static_version
 * is stored in a server-side file cache for quick retreival.
 *
 * Important Note: This assumes that running
 * `bin/cake cache clear_all`
 * is a part of our deployment script/process!
 * The server-side cache won't invalidate itself.
 */
trait StaticVersionHtmlHelperTrait
{
    /**
     * Hash value to append to requests for static assets
     *
     * @var string
     */
    private $static_version = '';

    /**
     * Generate directory listing on Windows
     *
     * @return string
     * @codeCoverageIgnore
     */
    protected function getWindowsDirList()
    {
        $dir_listing = shell_exec('dir /s ' . WWW_ROOT);

        // strip summary section from dir listing so we generate
        // a consistent hash when the content has not changed
        $lines = explode("\n", $dir_listing);
        $lines = array_slice($lines, 0, count($lines)-5);
        $dir_listing = implode("\n", $lines);

        return $dir_listing;
    }

    /**
     * Generate directory listing on *nix
     *
     * @return string
     */
    protected function getLinuxDirList()
    {
        return shell_exec('ls -lR ' . WWW_ROOT);
    }

    /**
     * Generate static version hash value
     *
     * Either grab it from the cache if it is already there
     * or generate a new one, write it to cache and return it
     *
     * @return string
     */
    protected function generateStaticVersion()
    {
        $cached_version = Cache::read('static_version', 'default');

        if ($cached_version === false) {
            // if there's no value already in the cache then generate one
            // a md5 of the directory listing (with timestamps) will suffice
            // @codeCoverageIgnoreStart
            if (DS == '\\') {
                // we are running on a windows host
                $dir_listing = $this->getWindowsDirList();
                // @codeCoverageIgnoreEnd
            } else {
                // we are on some kind of linux/unix-like os
                $dir_listing = $this->getLinuxDirList();
            }
            $static_version = md5($dir_listing);

            // write it to cache
            Cache::write('static_version', $static_version, 'default');

            // and return it
            return $static_version;
        } else {
            // else used the cached value
            return $cached_version;
        }
    }

    /**
     * Getter for static_version property
     *
     * @return string
     */
    public function getStaticVersion()
    {
        /*
         * Ensure we either hit the disk cache or generate the hash value
         * only once per request and then store it in memory for later use
         *
         * This is much more efficient than hitting the disk cache
         * every time we call ->css() or ->script().
         */
        if ($this->static_version == '') {
            $this->static_version = $this->generateStaticVersion();
        }

        return $this->static_version;
    }

    /**
     * Append static_version to input path
     *
     * @param string $path Path
     * @return string
     */
    protected function appendHash($path)
    {
        return $path . "?hash={$this->getStaticVersion()}";
    }

    /**
     * Creates one or many style/link tags for CSS stylesheets.
     *
     * Overrides \Cake\View\Helper\HtmlHelper->css()
     * when trait is used in a ViewHelper which extends
     * \Cake\View\Helper\HtmlHelper
     *
     * @param string|array $path The name of a CSS style sheet or an array containing names of
     *   CSS stylesheets. If `$path` is prefixed with '/', the path will be relative to the webroot
     *   of your application. Otherwise, the path will be relative to your CSS path, usually webroot/css.
     * @param array $options Array of options and HTML arguments.
     * @return string|null CSS `link` or `style` tags, depending on the type of link.
     */
    public function css($path, array $options = [])
    {
        if ((array_key_exists('versioned', $options)) && ($options['versioned'])) {
            unset($options['versioned']);
            if (is_array($path)) {
                foreach ($path as $key => $value) {
                    $path[$key] = $this->appendHash($value);
                }
            } else {
                $path = $this->appendHash($path);
            }
        }
        return parent::css($path, $options);
    }

    /**
     * Creates one or many `script` tags for javascript files.
     *
     * Overrides \Cake\View\Helper\HtmlHelper->script()
     * when trait is used in a ViewHelper which extends
     * \Cake\View\Helper\HtmlHelper
     *
     * @param string|array $url String or array of javascript files to include
     * @param array $options Array of options and HTML attributes
     * @return string|null String of `script` tags or null if block is specified in options
     *   or if $once is true and the file has been included before.
     */
    public function script($url, array $options = [])
    {
        if ((array_key_exists('versioned', $options)) && ($options['versioned'])) {
            unset($options['versioned']);
            if (is_array($url)) {
                foreach ($url as $key => $value) {
                    $url[$key] = $this->appendHash($value);
                }
            } else {
                $url = $this->appendHash($url);
            }
        }
        return parent::script($url, $options);
    }
}
