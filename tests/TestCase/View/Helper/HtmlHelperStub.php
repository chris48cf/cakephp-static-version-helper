<?php
namespace Chris48cf\StaticVersionHelper\Test\TestCase\View\Helper;

use Cake\View\Helper\HtmlHelper;
use Chris48cf\StaticVersionHelper\View\Helper\StaticVersionHtmlHelperTrait;

/**
 * Stub class for testing StaticVersionHtmlHelperTrait
 *
 * We can't directly instantiate a trait, so we need
 * a minimal helper implementation in order to test it
 */
class HtmlHelperStub extends HtmlHelper
{
    use StaticVersionHtmlHelperTrait;

    public $fakeDirList = 'derpderpderp';

    /**
     * Mock out getLinuxDirList() method to return a predictable result
     *
     * @return string
     */
    protected function getLinuxDirList()
    {
        return $this->fakeDirList;
    }

    /**
     * Mock out getWindowsDirList() method to return a predictable result
     *
     * @return string
     */
    protected function getWindowsDirList()
    {
        return $this->fakeDirList;
    }
}
