[![Run Status](https://api.shippable.com/projects/57a0b74e5685720c00669a0d/badge?branch=master)](https://app.shippable.com/projects/57a0b74e5685720c00669a0d) [![Coverage Badge](https://api.shippable.com/projects/57a0b74e5685720c00669a0d/coverageBadge?branch=master)](https://app.shippable.com/projects/57a0b74e5685720c00669a0d)

# CakePHP Static Version Helper

A CakePHP 3 Helper for versioning and cache-busing static assets.

When used in a View Helper which extends `\Cake\View\Helper\HtmlHelper`, this trait overrides the standard `css()` and `script()` methods with versions that can optionally accept a key `'versioned'` to ensure that when we change static css + js, browsers download new versions instead of using cached assets.

This is achieved by generating a hash value based on a directory listing of `WWW_ROOT` (including last modified timestamps). This means we'll get a new hash value when something in `WWW_ROOT` changes. Once generated, the hash is stored in a server-side file cache for quick retrieval.

## Platform Support

* Supported PHP versions: 5.4, 5.5 and 5.6.
* This plugin relies on a call to `shell_exec()`. It has been tested on Linux and Windows hosts. It might also work on a MacOS host but this is untested.

## Installation

Install with composer:

```
"repositories": [
    {
        "type": "vcs",
        "url": "https://bitbucket.org/chris48cf/cakephp-static-version-helper"
    }
],
"require": {
    "chris48cf/cakephp-static-version-helper": "dev-master"
}
```

## Usage

* Add the code `Plugin::load('Chris48cf/StaticVersionHelper');` to your project's `bootstrap.php`.
* Create a view helper in your project which extends `Cake\View\Helper\HtmlHelper`
* Pull in the `StaticVersionHtmlHelperTrait`. This will override the built-in `css()` and `script()` methods

For example:

```php
<?php
namespace App\View\Helper;

use Cake\View\Helper\HtmlHelper;
use Chris48cf\StaticVersionHelper\View\Helper\StaticVersionHtmlHelperTrait;

class MyHtmlHelper extends HtmlHelper
{
    use StaticVersionHtmlHelperTrait;
}
```

* We can also extend another class which itself extends `Cake\View\Helper\HtmlHelper`. For example, if we are using the `friendsofcake/bootstrap-ui` plugin:

```php
<?php
namespace App\View\Helper;

use BootstrapUI\View\Helper\HtmlHelper;
use Chris48cf\StaticVersionHelper\View\Helper\StaticVersionHtmlHelperTrait;

class MyHtmlHelper extends HtmlHelper
{
    use StaticVersionHtmlHelperTrait;
}
```

* Your helper can now be loaded into a view. For example, to load into your `AppView`:

```php
<?php
namespace App\View;

use Cake\View\View;

class AppView extends View
{
    public function initialize()
    {
        $this->loadHelper('Html', ['className' => 'MyHtml']);
    }
}
```

* Now we can call `$this->Html->css()` or `$this->Html->script()` in a template and optionally pass `'versioned' => true` in the `$options` array to version an asset.

```php
<?= $this->Html->css('foo.css', ['versioned' => true]) ?>

<?= $this->Html->script('bar.js', ['versioned' => true]) ?>
```

* If there are assets which we don't expect to change or already include versioning in the file name, we can include them without versioning.

```php
<?= $this->Html->script('js/bootstrap.3.3.7.min.js'); ?>
```

## Notes

This plugin assumes that clearing the default cache will be a part of your application's deployment process. If using CakePHP 3.3 or higher, this can be automated using the [Cache Shell](http://api.cakephp.org/3.3/class-Cake.Shell.CacheShell.html). If using CakePHP 3.2 or below, the cache shell is available as a [plugin](https://bitbucket.org/chris48cf/cakephp-cache-shell-shim). The server-side cache will not invalidate itself!
